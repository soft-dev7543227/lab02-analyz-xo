package com.mycompany.lab02;

import java.util.Scanner;

public class Lab02 {

    static char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row;
    static int col;

    static void printWelcome() {
        System.out.println("Welcome to Ox Game.");
    }

    static void printBoard() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.print("| " + board[row][col] + " ");
            }
            System.out.println("|\n-------------");
        }
    }

    static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print(currentPlayer + " turn." + " " + "Please input row[1-3] and column[1-3] : ");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            if (board[row][col] == '-') {
                board[row][col] = currentPlayer;
                break;
            }
        }
    }

    static void switchPlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    static boolean isWin() {
        if (checkRow() || checkCol() || checkDiagonal()) {
            return true;
        }
        return false;
    }

    static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[row][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (board[i][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    static boolean checkDiagonal() {
        // check L to R
        if (board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer) {
            return true;
        }

        // R to L
        if (board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer) {
            return true;
        }

        return false;
    }

    static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    static void printWin() {
        System.out.println(currentPlayer + " Win!!!!");
    }

    static void printDraw() {
        System.out.println("Draw!!");
    }

    static void resetGame() {
        currentPlayer = 'X';
        board = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    }

    static boolean inputContinue() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Continue? (y/n): ");
        String input = sc.nextLine();
        return input.equalsIgnoreCase("y");
    }

    public static void main(String[] args) {
        printWelcome();
        boolean continueGame = true;

        while (continueGame) {
            printBoard();
            inputRowCol();

            if (isWin()) {
                printBoard();
                printWin();
                continueGame = inputContinue();
                if (continueGame) {
                    resetGame();
                }
            } else if (isDraw()) {
                printBoard();
                printDraw();
                continueGame = inputContinue();
                if (continueGame) {
                    resetGame();
                }
            } else {
                switchPlayer();
            }
        }
    }

}
